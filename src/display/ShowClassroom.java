package display;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class ShowClassroom
{
     
    String error;
    List<Object> allData=new ArrayList<Object>();
     
    public String room,room_code,location,projector,type;
    int capacity;
    Object obj=new Object();
 
    public void setObj(Object obj) {
        this.obj = obj;
    }
    public String getError() {
        return error;
    }
    public List getDb_Data()
    {
        int i=0;
        try
        {		Connection con;
                String qry;
                try {
      			  // Provide database Driver according to your database
      			   Class.forName("org.postgresql.Driver");
      			   
      			   // Provide URL, database and credentials according to your database 
      			   con= DriverManager.getConnection("jdbc:postgresql://172.16.40.26:5432/student?currentSchema=timetable", "student","student");
      		  } catch (Exception e) {
      			   e.printStackTrace();
      			   return null;
      		  }
      		  if(con != null){
      			  System.out.println("Connection created successfully....");
      		  }            
                Statement s = con.createStatement();                
                qry="SELECT room,room_code,location,capacity,projector,type "+"from classroom ";
                ResultSet r=s.executeQuery(qry);
                while(r.next())
                {
                    DataFields d=new DataFields(r.getString(1), r.getString(2), r.getString(3),r.getString(6), r.getString(5),r.getInt(4));                    
                    allData.add(i,d);
                    i++;
                }
                 
        }
        catch(Exception ex)
        {
                error="<b>Contact Administrator :</b><br/>" + ex;
                
                System.out.println("Your query is not working " + ex);
        }
       return allData;
    }
    public String getRoomName()
    {
        this.room=((DataFields)obj).room;
        return this.room;
    }
    public String getRoomCode() {
        this.room_code=((DataFields)obj).room_code;
        return this.room_code;
    }
 
    public String getRoomLocation() {
        this.location=((DataFields)obj).location;
        return this.location;
    }
    public int getRoomCapacity() {
        this.capacity=((DataFields)obj).capacity;
        return this.capacity;
    }
    public String getProjectorAvailability() {
        this.projector=((DataFields)obj).projector;
        return this.projector;
    }
    public String getRoomType() {
        this.type=((DataFields)obj).type;
        return this.type;
    }
 
    public class DataFields
    {
    	private String room;
    	private String room_code;
    	private String location;
    	private String projector;
    	private String type;
    	private int capacity;
 
        public DataFields(String  room,String  room_code,String  location,String  projector,String type,int capacity)
        {
            this.room=room;
            this.room_code=room_code;
            this.type=type;
            this.location=location;
            this.projector=projector;
            this.capacity=capacity;
        }
    }
    
}